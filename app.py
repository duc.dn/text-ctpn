# coding=utf-8
# Copyright 2019 VNP-AI TEAM
# By Duc_DN

import numpy as np
import time
import os
import time
import cv2
from PIL import Image
import pytesseract
import tensorflow as tf
from nets import model_train as model
from utils.rpn_msr.proposal_layer import proposal_layer
from utils.text_connector.detectors import TextDetector
import glob
# Check the number of images already in the folder

def resize_image(img):
    img_size = img.shape
    im_size_min = np.min(img_size[0:2])
    im_size_max = np.max(img_size[0:2])

    im_scale = float(600) / float(im_size_min)
    if np.round(im_scale * im_size_max) > 1200:
        im_scale = float(1200) / float(im_size_max)
    new_h = int(img_size[0] * im_scale)
    new_w = int(img_size[1] * im_scale)

    new_h = new_h if new_h // 16 == 0 else (new_h // 16 + 1) * 16
    new_w = new_w if new_w // 16 == 0 else (new_w // 16 + 1) * 16

    re_im = cv2.resize(img, (new_w, new_h), interpolation=cv2.INTER_LINEAR)
    return re_im, (new_h / img_size[0], new_w / img_size[1])

def detectText(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.threshold(gray, 0, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    name = time.time()
    name_path = 'img_temp/'+str(name)+'.jpg'
    cv2.imwrite(name_path, gray)
    text = pytesseract.image_to_string(Image.open(name_path),lang='vie',config='--psm 13 --oem 3 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYabcdeghiklmnopqrstuvxyzÂÊÔàáâãèéêìíòóôõùúýăĐđĩũƠơưạảấầẩậắằẵặẻẽếềểễệỉịọỏốồổỗộớờởỡợụủỨứừửữựỳỵỷỹ')
    # os.remove(name_path)
    return text

with tf.get_default_graph().as_default():
    input_image = tf.placeholder(tf.float32, shape=[None, None, None, 3], name='input_image')
    input_im_info = tf.placeholder(tf.float32, shape=[None, 3], name='input_im_info')
    bbox_pred, cls_pred, cls_prob = model.model(input_image)
    global_step = tf.get_variable('global_step', [], initializer=tf.constant_initializer(0), trainable=False)
    variable_averages = tf.train.ExponentialMovingAverage(0.997, global_step)
    saver = tf.train.Saver(variable_averages.variables_to_restore())
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
    saver.restore(sess, "checkpoints_mlt/ctpn_50000.ckpt")

im = cv2.imread('d.jpg')[:,:,::-1]
img, (rh, rw) = resize_image(im)
h, w, c = img.shape
im_info = np.array([h, w, c]).reshape([1, 3])
bbox_pred_val, cls_prob_val = sess.run([bbox_pred, cls_prob],
                                        feed_dict={input_image: [img],
                                                    input_im_info: im_info})

textsegs, _ = proposal_layer(cls_prob_val, bbox_pred_val, im_info)
scores = textsegs[:, 0]
textsegs = textsegs[:, 1:5]
textdetector = TextDetector(DETECT_MODE='H')
boxes = textdetector.detect(textsegs, scores[:, np.newaxis], img.shape[:2])
boxes = np.array(boxes, dtype=np.int)
my_sorted_list = sorted(boxes, key=lambda x: x[1])
print(my_sorted_list)
for i, box in enumerate(my_sorted_list):
    crop = img[int(box[1]-5):int(box[5]+5),int(box[0]-5):int(box[4]+5)]
    text = detectText(crop)
    print(text)
    if text is not '':
        cv2.polylines(img, [box[:8].astype(np.int32).reshape((-1, 1, 2))], True, color=(0, 255, 0),
                        thickness=2)
        # cv2.imshow('a',img[:, :, ::-1])
        # cv2.waitKey(0)

# img = cv2.resize(img, None, None, fx=1.0 / rh, fy=1.0 / rw, interpolation=cv2.INTER_LINEAR)
# cv2.imwrite('1.jpg',img[:, :, ::-1])